

nextflow.enable.dsl = 2

process fastP1 {
        publishDir "${params.outdir1}_fastP", mode: "copy", overwrite: true   
        container "https://depot.galaxyproject.org/singularity/fastp:0.22.0--h2e03b76_0"
    
        input:
                path fastPchannel1
        output:
                path "${fastPchannel1.getSimpleName()}_trim.fastq", emit: fastq_files1
                path "*fastp.json", emit: json_files1
                path "*fastp.html", emit: html_files1
        script:
        """
        fastp -i ${fastPchannel1} -o ${fastPchannel1.getSimpleName()}_trim.fastq -j ${fastPchannel1.getSimpleName()}_trim.fastp.json

        """

}


process srst21{
        publishDir "${params.outdir1}/srst2_sample1", mode: "copy", overwrite: true   
        container "https://depot.galaxyproject.org/singularity/srst2%3A0.2.0--py27_2"
    
        input:
                path inchannel // durch combine() ist es nur noch ein Channel mit jeweils einem Array [fastq-Datei, CARD-fasta-Datei]
        output:
                path "*"  //inhalt fuer den oben geschaffenen ordner
        script: // hier muss man dann auf die FASTQ- und die FASTA-Datei jeweils über inchannel[Position] zugreifen - die Position sieht man im view() unten
        """
        srst2 --input_se ${inchannel[0]} --output Patient1 --gene_db ${inchannel[1]}
        """

}

process fastQC1{
        publishDir "${params.outdir1}/fastqc_sample1", mode: "copy", overwrite: true   
        container "https://depot.galaxyproject.org/singularity/fastqc:0.11.9--hdfd78af_1"
    
        input:
                path fastqc_channel1
        output:
                path "*"  
        script:
        """
        
        fastqc ${fastqc_channel1}
        """

}


workflow {
  pat1 = channel.fromPath("${params.infiles}/*.fastq") // Dann sind alle fastq-Dateien in einem Channel
  fastPchannel1 = fastP1(pat1)
  card_channel1 = channel.fromPath(params.card)
  srst2_channel1 = fastPchannel1.fastq_files1
  // Siehe https://www.nextflow.io/docs/latest/operator.html#combine - mit .combine umgeht man das Problem, dass die
  // CARD-Referenz nur ein Mal in dem Channel steht und srst2 dann nur ein Mal läuft (zur Veranschaulichung habe ich ein
  // view() dazugeschrieben, schau dir mal die Ausgabe beim Aufruf an)
  srst2_inchannel = srst2_channel1.combine(card_channel1)  
  srst2_inchannel.view()
  resistenz1 = srst21(srst2_inchannel)
  fastqc_channel1 = fastQC1(fastPchannel1.fastq_files1)
}
